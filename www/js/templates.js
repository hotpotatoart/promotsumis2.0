// Templates essentials jquery


var onAnimation = false;
var customized = false;

$(document).ready(function(){

	adjustBody();
	
	$("#btnAceptPrompt2").click( evt => {
		if(!customized){
			setTimeout(hideMessage, 300);
			setTimeout(()=>{
				onAnimation = false;
			}, 1500);	
		}
					
	});

	$("#btnAceptPrompt").click( evt => {
		if(!customized){
			setTimeout(hideMessage, 300);
			setTimeout(()=>{
				onAnimation = false;
			}, 1500);
		}		
	});

	$("#btnCancelPrompt").click( evt => {
		if(!customized){
			setTimeout(hideMessage, 300);
			setTimeout(()=>{
				onAnimation = false;
			}, 1500);
		}			
	});	

	$(".btnOpacity").click( evt => {
		var tag = "#" + evt.target.id;
		$(tag).css("opacity", "0.6");
		setTimeout(()=>{
			$(tag).css("opacity", "1.0");
		}, 200);		
	});

	$(".btnScale").click( evt => {
		var tag = "#" + evt.target.id;
		$(tag).css("transform", "scale(0.9)");
		setTimeout(()=>{
			$(tag).css("transform", "scale(1.0)");
		}, 200);		
	});

	$(window).resize( event => {
		adjustBody();
	});

});

function msnConfirm(title, body, callback){	
	callback();
	showMessage(title, body, "#multiButtonPrompt");		
}// fin de la funcion mensaje confirmar

function msnAlert(title, body, callback){
	callback();
	showMessage(title, body, "#singleButtonPrompt");		
}// fin de la funcion mensaje de confirmacion

function showMessage(title, body, messageTag){
	onAnimation = true;
	$(messageTag).css("display", "block");
	$("#titlePrompr").text(title);
	$("#bodyPrompt").text(body);	
	$(".backgroundPrompt").css("animation-name", "showBackground");
	setTimeout(()=>{
		$(".backgroundPrompt").css("height", "150%");		
		$(".boxPrompt").css("animation-name", "showBoxPrompt");
	}, 450);
	setTimeout(()=>{
		$(".boxPrompt").css("transform", "scale(1.0)");			
	}, 900);		
}// fin de la funcion mostrar mensaje

function hideMessage(){	
	$(".boxPrompt").css("animation-name", "hideBoxPrompt");		
	setTimeout(()=>{
		$(".boxPrompt").css("transform", "scale(0.0)");
		$(".buttonsPrompt").css("display", "none");
		$(".backgroundPrompt").css("animation-name", "hideBackground");
	}, 450);
	setTimeout(()=>{
		$(".backgroundPrompt").css("height", "0%");				
	}, 900);	
}// fin de la funcion ocultar mensaje

function updateStatusBar(so){
    if (so == "iOS") 
        $("#statusBar").css("height", "30px")
}// fin de la funcion actualizar status bar

function adjustBody(){
	$(".adjustBody").css("background-size", "" +
	$(window).width() + "px " + $(window).height() + "px");		
}// fin de la funcion ajustar body
