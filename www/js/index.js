
var userData = {user: "", password: "", puntos: 0};
var states = [];
var dataProducts = [
    {codeProduct: "SabNat", nameProduct: "Sabritas naturales", cantidad: 0, nameImg: "Sabritas.png"},
    {codeProduct: "RufQue", nameProduct: "Ruffles de queso", cantidad: 0, nameImg: "Ruffles.png"},
    {codeProduct: "DorNac", nameProduct: "Doritos nachos", cantidad: 0, nameImg: "Doritos.png"},
    {codeProduct: "Cru", nameProduct: "Crujitos", cantidad: 0, nameImg: "Crujitos.png"},
    {codeProduct: "Che", nameProduct: "Chetos", cantidad: 0, nameImg: "Chetos.png"},
    {codeProduct: "Fri", nameProduct: "Fritos sal y limon", cantidad: 0, nameImg: "Fritos.jpg"},
    {codeProduct: "Pak", nameProduct: "Paketaxo", cantidad: 0, nameImg: "Paketaxo.jpg"}    
];

function initComponents(){
    NativeStorage.getItem("userDataTentero", getUserData, getUserDataError);
    NativeStorage.getItem("dataProducts", getDataProduct, getDataProductError);
}// fin de la funcion iniciar componentes

function getDataProduct (obj) {
    window.dataProducts = obj; 
}// fin de la funcion get filters

function getDataProductError (error) {
    if(error.code == 2) {
        NativeStorage.setItem("dataProducts", dataProducts, setDataProduct, setDataProductError);
    } else {
        alert("Error:" + error.code);
        if (error.exception !== "") 
            alert("Excepción: " + error.exception);
    }              
}// fin de la funcion getError  

function setDataProduct (obj) {
    //userData = obj;
}// fin de la funcion get filters

function setDataProductError (error) {
    if(error.code == 2) {
        NativeStorage.setItem("dataProducts", userData, setUserData, setUserDataError);
    } else {
        alert("Error:" + error.code);
        if (error.exception !== "") 
            alert("Excepción: " + error.exception);
    }              
}// fin de la funcion getError 

function getUserData (obj) {
    window.userData = obj; 
}// fin de la funcion get filters

function getUserDataError (error) {
    if(error.code == 2) {
        NativeStorage.setItem("userDataTentero", userData, setUserData, setUserDataError);
    } else {
        alert("Error:" + error.code);
        if (error.exception !== "") 
            alert("Excepción: " + error.exception);
    }              
}// fin de la funcion getError  

function setUserData (obj) {
    //userData = obj;
}// fin de la funcion get filters

function setUserDataError (error) {
    if(error.code == 2) {
        NativeStorage.setItem("userDataTentero", userData, setUserData, setUserDataError);
    } else {
        alert("Error:" + error.code);
        if (error.exception !== "") 
            alert("Excepción: " + error.exception);
    }              
}// fin de la funcion getError 

function initComponentsRegister(idElement){
    fillStates(idElement);//  enviar id del elemento al que sea agregaran los estados
}// fin de la funcion inicializar componentes

function updatePage(){    
    if(userData.user != "" && userData.password != ""){
            location.href = "menu.html";
    } else {
        location.href = "login.html";
    }    
}// fin de la funcion update page

function fillStates(idTag){
    //var jsonStates = '[{ "id": 1, "name": "Aguascalientes" },{ "id": 2, "name": "Baja California" },{ "id": 3, "name": "Baja California Sur" },{ "id": 4, "name": "Campeche" },{ "id": 5, "name": "Coahuila" },{ "id": 6, "name": "Colima" },{ "id": 7, "name": "Chiapas" },{ "id": 8, "name": "Chihuahua" },{ "id": 9, "name": "CDMX" },{ "id": 10, "name": "Durango" },{ "id": 11, "name": "Guanajuato" },{ "id": 12, "name": "Guerrero" },{ "id": 13, "name": "Hidalgo" },{ "id": 14, "name": "Jalisco" },{ "id": 15, "name": "Máxico" },{ "id": 16, "name": "Michoacán" },{ "id": 17, "name": "Morelos" },{ "id": 18, "name": "Nayarit" },{ "id": 19, "name": "Nuevo León" },{ "id": 20, "name": "Oaxaca" },{ "id": 21, "name": "Puebla" },{ "id": 22, "name": "Querétaro" },{ "id": 23, "name": "Quintana Roo" },{ "id": 24, "name": "San Luis Potosí­" },{ "id": 25, "name": "Sinaloa" },{ "id": 26, "name": "Sonora" },{ "id": 27, "name": "Tabasco" },{ "id": 28, "name": "Tamaulipas" },{ "id": 29, "name": "Tlaxcala" },{ "id": 30, "name": "Veracruz" },{ "id": 31, "name": "Yucatán" },{ "id": 32, "name": "Zacatecas" }]';
    var jsonStates = '[{ "id": 1, "name": "Aguascalientes" },{ "id": 2, "name": "Baja California" },{ "id": 3, "name": "Baja California Sur" },{ "id": 4, "name": "Campeche" },{ "id": 5, "name": "Coahuila" },{ "id": 6, "name": "Colima" },{ "id": 7, "name": "Chiapas" },{ "id": 8, "name": "Chihuahua" },{ "id": 9, "name": "CDMX" },{ "id": 10, "name": "Durango" },{ "id": 11, "name": "Guanajuato" },{ "id": 12, "name": "Guerrero" },{ "id": 13, "name": "Hidalgo" },{ "id": 14, "name": "Jalisco" },{ "id": 15, "name": "Mexico" },{ "id": 16, "name": "Michoacan" },{ "id": 17, "name": "Morelos" },{ "id": 18, "name": "Nayarit" },{ "id": 19, "name": "Nuevo Leon" },{ "id": 20, "name": "Oaxaca" },{ "id": 21, "name": "Puebla" },{ "id": 22, "name": "Queretaro" },{ "id": 23, "name": "Quintana Roo" },{ "id": 24, "name": "San Luis Potosí­" },{ "id": 25, "name": "Sinaloa" },{ "id": 26, "name": "Sonora" },{ "id": 27, "name": "Tabasco" },{ "id": 28, "name": "Tamaulipas" },{ "id": 29, "name": "Tlaxcala" },{ "id": 30, "name": "Veracruz" },{ "id": 31, "name": "Yucatan" },{ "id": 32, "name": "Zacatecas" }]';
    states = JSON.parse(jsonStates);
    for(var i = 0; i < states.length; i++){
        $("#" + idTag).append('<li><a href="#" class="stateOption">' + states[i].name + '</a></li>');
    }
}// fin de la funcion llenar estados

/////////////////////////////////////////////////////////////////////////////////

function pressButton(id){
    $("#"+id).css("opacity", "0.7");
    $("#"+id).css("transform", "scale(0.9)");
    setTimeout(()=>{
        $("#"+id).css("opacity", "1.0");
        $("#"+id).css("transform", "scale(1)");        
    }, 200);
}// fin de la funcion press button

function pressMe(targetId){
    $("#"+targetId).css("opacity", "0.7");
    setTimeout(()=>{
        $("#"+targetId).css("opacity", "1.0");
    }, 200);

}// fin de la funcion presioname

function getDataForm(){
    
    var dataForm = {
        nombres: $("#RNombres").val(),
        apellidos: $("#RApellidos").val(),
        calle_numero: $("#RDireccion").val(),
        colonia: $("#RColonia").val(),
        delegacion: $("#RDelegacion").val(),
        estado: $("#btnState").text(),
        cp: $("#RCP").val(),
        telefono: $("#RTelefono").val(),
        celular: $("#RCelular").val(),
        email: $("#REmail").val(),
        usuario_tendero: $("#RUsuario").val(),
        password_tendero: $("#RPassword").val(),
        clave_tiendita: infoRepartidor.codigoRepartidor,
        clave_repartidor: infoRepartidor.claveTiendita,
        puntos: 0
    };                
    if(dataForm.nombres == "" || dataForm.apellidos == ""){
        return -1;
    }
    if(dataForm.colonia == "" || dataForm.calle_numero == ""){
        return -1;
    }
    if(dataForm.delegacion == "" || dataForm.estado == ""){
        return -1;
    }
    if(dataForm.cp == "" || dataForm.telefono == ""){
        return -1;
    }
    if(dataForm.celular == "" || dataForm.email == ""){
        return -1;
    }
    if(dataForm.usuario_tendero == "" || dataForm.password_tendero == ""){
        return -1;
    }
    if(dataForm.clave_tiendita == "" || dataForm.clave_repartidor == "" ){
        return -1;
    }
    dataForm = JSON.stringify(dataForm);        
    return dataForm;
}// fin de la funcion obtener datos de formulario

function loadPrizesData(){
    var premios = [{
            nombre:"Batidora Taurus",
            puntos: 310,
            existencia: 10,
            canjeado: false,
            imgURL: "https://hotpotatoart.com/PromoTsumis/premios/batidoraTaurus.png"
        }, {
            nombre:"Cafetera B&D",
            puntos: 460,
            existencia: 10,
            canjeado: false,
            imgURL: "https://hotpotatoart.com/PromoTsumis/premios/cafeteraB&D.png"
        }, {
            nombre:"Enfriador de agua mabe",
            puntos: 410,
            existencia: 10,
            canjeado: false,
            imgURL: "https://hotpotatoart.com/PromoTsumis/premios/enfriadorDeAgua.png"
        }, {
            nombre:"Estufa de 2 quemadores",
            puntos: 260,
            existencia: 10,
            canjeado: false,
            imgURL: "https://hotpotatoart.com/PromoTsumis/premios/estufa2.png"
        }, {
            nombre:"Exprimidor Taurus",
            puntos: 170,
            existencia: 10,
            canjeado: false,
            imgURL: "https://hotpotatoart.com/PromoTsumis/premios/exprimidorTaurus.png"
        }, {
            nombre:"Horno electrico B&D",
            puntos: 380,
            existencia: 10,
            canjeado: false,
            imgURL: "https://hotpotatoart.com/PromoTsumis/premios/hornoElectricoB&D.png"
        }, {
            nombre:"Juego de cubiertos",
            puntos: 70,
            existencia: 10,
            canjeado: false,
            imgURL: "https://hotpotatoart.com/PromoTsumis/premios/juegoDeCubiertos.png"
        }, {
            nombre:"Juego de cuchillos",
            puntos: 90,
            existencia: 10,
            canjeado: false,
            imgURL: "https://hotpotatoart.com/PromoTsumis/premios/juegoDeCuchillos.png"
        }, {
            nombre:"Juego de topers",
            puntos: 140,
            existencia: 10,
            canjeado: false,
            imgURL: "https://hotpotatoart.com/PromoTsumis/premios/juegoDeTopers.png"
        }, {
            nombre:"Juego de utensilios",
            puntos: 125,
            existencia: 10,
            canjeado: false,
            imgURL: "https://hotpotatoart.com/PromoTsumis/premios/juegoDeUtensilios.png"
        }, {
            nombre:"Juego de vasos",
            puntos: 100,
            existencia: 10,
            canjeado: false,
            imgURL: "https://hotpotatoart.com/PromoTsumis/premios/juegoDeVasos.png"
        }, {
            nombre:"Licuadora Oster",
            puntos: 240,
            existencia: 10,
            canjeado: false,
            imgURL: "https://hotpotatoart.com/PromoTsumis/premios/licuadoraOster.png"
        }, {
            nombre:"Microondas",
            puntos: 890,
            existencia: 10,
            canjeado: false,
            imgURL: "https://hotpotatoart.com/PromoTsumis/premios/microOndas.png"
        }, {
            nombre:"Mini componente Philips",
            puntos: 1100,
            existencia: 10,
            canjeado: false,
            imgURL: "https://hotpotatoart.com/PromoTsumis/premios/miniComponentePhilips.png"
        }, {
            nombre:"Pantalla 19 pulgadas",
            puntos: 1200,
            existencia: 10,
            canjeado: false,
            imgURL: "https://hotpotatoart.com/PromoTsumis/premios/pantalla19.png"
        }, {
            nombre:"Parrilla electrica de 1 quemador",
            puntos: 270,
            existencia: 10,
            canjeado: false,
            imgURL: "https://hotpotatoart.com/PromoTsumis/premios/parrillaElectrica1.png"
        }, {
            nombre:"Plancha Oster",
            puntos: 370,
            existencia: 10,
            canjeado: false,
            imgURL: "https://hotpotatoart.com/PromoTsumis/premios/planchaOster.png"
        }, {
            nombre:"Sandwichera Taurus",
            puntos: 330,
            existencia: 10,
            canjeado: false,
            imgURL: "https://hotpotatoart.com/PromoTsumis/premios/sandwicheraTaurus.png"
        }, {
            nombre:"Sarten",
            puntos: 95,
            existencia: 10,
            canjeado: false,
            imgURL: "https://hotpotatoart.com/PromoTsumis/premios/sarten.png"
        }, {
            nombre:"Sillon",
            puntos: 960,
            existencia: 10,
            canjeado: false,
            imgURL: "https://hotpotatoart.com/PromoTsumis/premios/sillon.png"
        }, {
            nombre:"Smart phone Motorola",
            puntos: 1300,
            existencia: 10,
            canjeado: false,
            imgURL: "https://hotpotatoart.com/PromoTsumis/premios/smartPhoneMotorola.png"
        }
    ];
    return premios;
}
